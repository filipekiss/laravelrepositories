<?php 

namespace Repovel\Repositories;
use Repovel\Contracts\RepositoryContract as RepositoryContract;
use Repovel\Contracts\CriteriaContract as CriteriaContract;
use Repovel\Repositories\Criterias\Criteria;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Container\Container as App;
use \DB;

/**
 * Class Repository
 * @package Repovel\Repositories;
 * @author Filipe Kiss
 */
abstract Class Repository implements RepositoryContract, CriteriaContract {
  
  private $app;
  
  protected $model;
  protected $with;
  protected $criteria;
  protected $skipCriteria = false;
  
  public function __construct(App $app, Collection $collection) {
    $this->app = $app;
    $this->criteria = $collection;
    $this->resetScope();
    $this->newModel();
  }
  
  abstract function model();
  
  /**
   * @param array $columns
   * @return mixed
   */
  public function all($columns = array('*')) 
  {
      $this->applyCriteria();
      $this->newQuery()->eagerLoadRelations();
      return $this->model->get($columns);
  }

  /**
   * @param int $perPage
   * @param array $columns
   * @return mixed
   */
  public function paginate($perPage = 15, $columns = array('*')) {
      $this->applyCriteria();
      $this->newQuery()->eagerLoadRelations();
      return $this->model->paginate($perPage, $columns);
  }

  /**
   * @param array $data
   * @return mixed
   */
  public function create(array $data) {
      return $this->model->create($data);
  }

  /**
   * @param array $data
   * @param $id
   * @param string $attribute
   * @return mixed
   */
  public function update(array $data, $id, $attribute="id") {
      return $this->model->where($attribute, '=', $id)->update($data);
  }

  /**
   * @param $id
   * @return mixed
   */
  public function destroy($id) {
      return $this->model->destroy($id);
  }
  
  public function delete($id) {
    return $this->destroy($id);
  }

  /**
   * @param $id
   * @param array $columns
   * @return mixed
   */
  public function find($id, $columns = array('*')) 
  {
      $this->applyCriteria();
      $this->newQuery()->eagerLoadRelations();
      return $this->model->find($id, $columns);
  }

  /**
   * @param $attribute
   * @param $value
   * @param array $columns
   * @return mixed
   */
  public function findBy($attribute, $value, $columns = array('*')) 
  {
      $this->applyCriteria();
      $this->newQuery()->eagerLoadRelations();
      return $this->model->where($attribute, '=', $value)->first($columns);
  }
  
  public function findLike($attribute, $value, $columns = array('*')) 
  {
      $this->applyCriteria();
      $this->newQuery()->eagerLoadRelations();
      return $this->model->where($attribute, 'LIKE', $value)->first($columns);
  }
  
  public function findRaw($attribute, $query, $columns = array('*'))
  {
    $this->applyCriteria();
    $this->newQuery()->eagerLoadRelations();
    return $this->model->where($attribute, DB::raw('('.$query.')'))->get($columns);
  }
  
  public function resetScope()
  {
    $this->skipCriteria(false);
    return $this;
  }
  
  public function skipCriteria($status = true)
  {
    $this->skipCriteria = $status;
    return $this;
  }
  
  public function getCriteria()
  {
    return $this->criteria;
  }
  
  public function getByCriteria(Criteria $criteria)
  {
    $this->model = $criteria->apply($this->model, $this);
    return $this;
  }
  
  public function pushCriteria(Criteria $criteria)
  {
    $this->criteria->push($criteria);
    return $this;
  }
  
  public function applyCriteria()
  {
    if ($this->skipCriteria === true)
      return true;

    foreach ($this->getCriteria() as $criteria) {
      if ($criteria instanceof Criteria)
        $this->model = $criteria->apply($this->model, $this);
    }
  }
  
  /**
   * @param string ... list of relations you wish to retrieve
   * @return Repository
   */
  public function with()
  {
    $relations = func_get_args();
    $this->with = $relations;
    return $this;
  }

  protected function eagerLoadRelations()
  {
    if(!is_null($this->with)) {
     foreach ($this->with as $relation) {
         $this->model->with($relation);
     }
    }

    return $this;
  }
  
  public function withTrashed($columns = array('*')) {
      return $this->model->withTrashed()->get($columns);
  }
  
  public function trashedOnly($columns = array('*')) {
      return $this->model->onlyTrashed()->get($columns);
  }
  
  public function newModel() {
    $model = $this->app->make($this->model());
    if (!$model instanceof Model) {
      throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
    }
    
    return $this->model = $model;
  }
  
  public function newQuery() {
    $this->model = $this->model->newQuery();
    return $this;
  }
  
  /**
   * This will always return a new model. This is to prevent the method returning a query after a "pushCriteria" for example.
   * @see getCurrentModel
   * @return Eloquent A empty Eloquent instance from the current model
   */
  public function getModel()
  {
    return $this->app->make($this->model());
  }
  
  /**
   * This will return the current model attribute. This may be an Eloquent or a Query Builder instance.
   * @see getModel
   * @return mixed An Eloquent or a Query instance
   */
  public function getCurrentModel()
  {
    return $this->model;
  }
}
