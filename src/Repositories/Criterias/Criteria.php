<?php namespace Repovel\Repositories\Criterias;

use Repovel\Contracts\RepositoryContract as Repository;
use Repovel\Contracts\RepositoryContract;

abstract class Criteria {

    /**
     * @param $model
     * @param RepositoryContract $repository
     * @return mixed
     */
    public abstract function apply($model, Repository $repository);
}