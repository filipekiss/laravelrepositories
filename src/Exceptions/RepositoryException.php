<?php 

namespace Repovel\Exceptions;

/**
 * Class RepositoryException
 * @package Repovel\Exceptions
 */
class RepositoryException extends \Exception {}